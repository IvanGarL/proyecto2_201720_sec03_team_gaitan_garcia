package model.data_structures;

import model.exceptions.ElementNotFoundException;

public class Queue<T>
{
	Node<T> first, last;

	public Queue()
	{
		first = null;
		last = null;

	}
	
	public void enqueue(T item) 
	{

		if(first == null)
		{
			first = new Node<T>(item);
			last = first;
		}
		else
		{
			Node<T> temp = new Node<T>(item);
			temp.changeNext(last);
			last = temp;
		}
	}

	public T dequeue() throws ElementNotFoundException
	{
		if( first == null ) throw new ElementNotFoundException("La lista ya se encuentra vacía");
		if( first.hasNext() ) 
		{
			T deleted = first.getElement();
			first = null;
			last = null;
			return deleted;
		}
		else
		{
			T deleted = first.getElement();
			Node<T> temp = first.getNext();
			first = temp;
			first.changePrevious(null);
			return deleted;
		}
	}

	public T getElementAtK(int pos) throws ElementNotFoundException
	{
		int size = getSize();
		T looked = null;
		if(pos > size - 1 || pos < 0) throw new ElementNotFoundException("La posicion se encuentra fuera de los limites de la lista.");
		if(first == null)
		{
			throw new ElementNotFoundException("La lista está vacía.");
		}
		else if( pos == 0)
		{
			looked = last.getElement();

		}
		else if(size - 1 == pos )
		{
			looked = first.getElement();
		}
		else
		{
			Node<T> temp = last;
			int current = 0;
			boolean stop = false;
			while(temp.hasNext() && !stop)
			{
				if(current == pos)
				{
					looked = temp.getElement();
					stop = true;
				}
				current++;
				temp = temp.getNext();
			}
		}
		return looked;
	}

	public int getSize()
	{
		if( first == null ) return 0;
		else
		{
			int cont = 1;
			Node<T> temp = last;
			while(temp.hasNext())
			{
				cont++;
				temp = temp.getNext();
			}
			return cont;
		}
	}
	
	public Node<T> getFirst() {
		// TODO Auto-generated method stub
		return first;
	}
	
	public Node<T> getLast() {
		// TODO Auto-generated method stub
		return last;
	}
	
	public StructuresIterator<T> iterator() {
		// TODO Auto-generated method stub
		return new StructuresIterator<T>(last);
	}

}
