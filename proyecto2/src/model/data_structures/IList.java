package model.data_structures;

import java.util.Comparator;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T>
{
	T getFirst();
	
    Object[] toArray( );
    
    <T> T[] toArray( T[] array );

	T getLast();
	
	void add( T elem );

	void addAtK( int pos, T elem );
		
	T getElementAtK(int pos);

	void deleteAtK(int pos);
	
	Integer getSize( );
	
	StructuresIterator<T> iterator();
	
	IList<T> sort(T[] array);

	
}
