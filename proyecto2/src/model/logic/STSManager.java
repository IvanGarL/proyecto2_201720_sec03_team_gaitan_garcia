package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import model.data_structures.HashTable;
import model.data_structures.List;
import model.data_structures.Queue;
import model.data_structures.StructuresIterator;
import model.vo.AgencyVO;
import model.vo.BusUpdateVO;
import model.vo.CalendarDateVO;
import model.vo.CalendarVO;
import model.vo.RouteVO;
import model.vo.ScheduleVO;
import model.vo.ShapeVO;
import model.vo.StopRegisterVO;
import model.vo.StopTimeVO;
import model.vo.StopVO;
import model.vo.TransferVO;
import model.vo.TripVO;
import api.ISTSManager;


public class STSManager implements ISTSManager{
	
	HashTable.SeparateChainHash<TransferVO, Integer> transfers = new HashTable.SeparateChainHash<TransferVO, Integer>();
	HashTable.SeparateChainHash<ShapeVO, Integer> shapes = new HashTable.SeparateChainHash<ShapeVO, Integer>();
	HashTable.SeparateChainHash<CalendarVO, Integer> calendar = new HashTable.SeparateChainHash<CalendarVO, Integer>();
	HashTable.SeparateChainHash<CalendarDateVO, Integer> calendarDates = new HashTable.SeparateChainHash<CalendarDateVO,Integer>();
	HashTable.SeparateChainHash<AgencyVO, String> agencies = new HashTable.SeparateChainHash<AgencyVO, String>();
	HashTable.SeparateChainHash<RouteVO, Integer> routes = new HashTable.SeparateChainHash<RouteVO, Integer>();
	HashTable.SeparateChainHash<BusUpdateVO, Integer> busesRoutes = new HashTable.SeparateChainHash<BusUpdateVO, Integer>();
	HashTable.SeparateChainHash<StopRegisterVO, String> stopRegisters = new HashTable.SeparateChainHash<StopRegisterVO, String>();
	HashTable<TripVO, Integer> trips = new HashTable.LinearHash<TripVO, Integer>();
	HashTable<StopTimeVO, String> stopTimes = new HashTable.SeparateChainHash<StopTimeVO, String>();
	HashTable<StopVO, Integer> stops = new HashTable.LinearHash<StopVO, Integer>();

	public void loadTransfers(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/transfers.txt")));
			reader.readLine();
			String line = reader.readLine();

			while(line != null){

				String data[] = line.split(",");
				String startStopId = data[0];
				String endStopId = data[1];
				Integer transferType = Integer.parseInt(data[2]);
				Integer transferTime;
				if(transferType != 0){
					transferTime = Integer.parseInt(data[3]);
				}
				else transferTime = 0;
				TransferVO nuevo = new TransferVO(startStopId, endStopId, transferType, transferTime);
				transfers.put(nuevo.getKey(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void loadShapes(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/shapes.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");
				Integer id = Integer.parseInt(data[0]);
				double shapeLat = Double.parseDouble(data[1]);
				double shapeLong = Double.parseDouble(data[2]);
				Integer sequence = Integer.parseInt(data[3]);
				Double disTraveled = Double.parseDouble(data[4]);
				
				ShapeVO nueva = new ShapeVO(id, shapeLat, shapeLong, sequence, disTraveled);
				shapes.put(nueva.getKey(), nueva);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void loadCalendar(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/calendar.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");
				Integer serviceId = Integer.parseInt(data[0]);
				boolean m = getBoolean(Integer.parseInt(data[1]));
				boolean t = getBoolean(Integer.parseInt(data[2]));
				boolean w = getBoolean(Integer.parseInt(data[3]));
				boolean th = getBoolean(Integer.parseInt(data[4]));
				boolean f = getBoolean(Integer.parseInt(data[5]));
				boolean s = getBoolean(Integer.parseInt(data[6]));
				boolean sn = getBoolean(Integer.parseInt(data[7]));
				Integer startDt = Integer.parseInt(data[8]);
				Integer endDt = Integer.parseInt(data[9]);
				CalendarVO nuevo = new CalendarVO(serviceId,m,t,w,th,f,s,sn,startDt,endDt);
				nuevo.loadDates(calendarDates);
				nuevo.loadTrips(trips);
				calendar.put(nuevo.getKey(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void loadCalendarDates(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/calendar_dates.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");
				Integer serviceId = Integer.parseInt(data[0]);
				Integer exceptionDt = Integer.parseInt(data[1]);
				Integer type = Integer.parseInt(data[2]);
				CalendarDateVO nuevo = new CalendarDateVO(serviceId,exceptionDt,type);
				nuevo.loadTrips(trips);
				calendarDates.put(nuevo.getKey(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	public void loadAgencies(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/agency.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");
				String id = data[0];
				String name = data[1];
				String url = data[2];
				String timeZone = data[3];
				String language = data[4];
				AgencyVO nuevo = new AgencyVO(id, name, url, timeZone, language);
				nuevo.loadRoutes(routes);
				agencies.put(nuevo.getKey(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void loadRoutes()
	{

		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/routes.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String agencyId = data[1];
				String routeNum = data[2];
				String routeName = data[3];
				String routeDesc = data[4];
				String routeUrl = data[5];
				RouteVO nuevo = new RouteVO(id, agencyId, routeNum, routeName, routeDesc, routeUrl, null, null);
				nuevo.loadTrips(trips);
				routes.put(nuevo.getKey(), nuevo);
				line = reader.readLine();

			}
			reader.close();

		} catch (Exception e) {
			// TODO: handle exception
		}		
	}
	
	public void loadTrips() 
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/trips.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int routeId = Integer.parseInt(data[0]);
				int serviceId = Integer.parseInt(data[1]);
				int tripId = Integer.parseInt(data[2]);
				String headSing = data[3];
				String tripShortName = data[4];
				String directionId = data[5];
				int blockId = Integer.parseInt(data[6]);
				String shapeId = data[7];
				int wheelchairFriendly = Integer.parseInt(data[8]);
				int bikeAllowed = Integer.parseInt(data[9]);

				TripVO nuevo = new TripVO(routeId, serviceId, tripId, headSing, 
						tripShortName, directionId, blockId, shapeId, bikeAllowed, wheelchairFriendly);
				trips.put(nuevo.getKey(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public void loadStopTimes()
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/stop_times.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String arrivalTimeData = data[1];
				String departureTimeData = data[2];
				int stopId = Integer.parseInt(data[3]);
				String stopSequence = data[4];
				String stopSign = data[5];
				int pickUp = Integer.parseInt(data[6]);
				int drop = Integer.parseInt(data[7]);

				double distance;
				if(data.length > 8) 
				{
					distance = Double.parseDouble(data[8]);
				}
				else distance = 0;
				StopTimeVO nuevo = new StopTimeVO(id, arrivalTimeData, departureTimeData, stopId, stopSequence, stopSign, pickUp, drop, distance);
				stopTimes.put(nuevo.getKey(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {

		}
	}
	public void loadStops() 
	{
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/stops.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");

				int id = Integer.parseInt(data[0]);
				int code;
				if(data[1] != null && !data[1].equals(" ") && !data[1].isEmpty())
				{					
					code= Integer.parseInt(data[1]);
				}
				else code = 0;
				String name = data[2];
				String stopDesc = data[3];
				double stopLat = Double.parseDouble(data[4]);
				double stopLon = Double.parseDouble(data[5]);
				String stopZone = data[6];
				int locationType = Integer.parseInt(data[8]);
				StopVO nuevo = new StopVO(id, code, name, stopDesc, stopLat, stopLon, stopZone, null, locationType, null);
				stops.put( nuevo.getKey(), nuevo );
				line = reader.readLine();
			}
			reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void readBusUpdate(File rtFile) 
	{
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();
		try 
		{			
			Object objects = (Object) parser.parse(new FileReader(rtFile));
			JSONArray list = (JSONArray) objects;
			for(Object temp: list)
			{
				JSONObject current = (JSONObject) temp;
				String id = (String) current.get("VehicleNo");
				int tripid = Integer.parseInt(current.get("TripId").toString());
				String routeNo = (String) current.get("RouteNo");
				String direction = (String) current.get("Direction");
				String destination = (String) current.get("Destination");
				String pattern = (String) current.get("Pattern");
				double lat = (Double) current.get("Latitude");
				double lon = (Double) current.get("Longitude");
				String time = (String) current.get("RecordedTime");
				String url = (String) ((JSONObject) current.get("RouteMap")).get("Href");
				BusUpdateVO nuevo = new BusUpdateVO(id, tripid, routeNo, direction, destination, pattern, lat, lon, time, url);
				busesRoutes.put(nuevo.getKey(), nuevo);
			}
		} catch (Exception e) {
		}
	}
	
	public void readStopRegistersUpdate(File rtFile)
	{
		JSONParser parser = new JSONParser();
		try{
			Object objects = (Object) parser.parse(new FileReader(rtFile));
			JSONArray list = (JSONArray) objects;
			for (Object temp : list) {
				JSONObject currentA = (JSONObject) temp;
				String RouteNo = (String) currentA.get("RouteNo");
				String RouteName = (String) currentA.get("RouteName");
				String direction = (String) currentA.get("Direction");
				String url = (String) ((JSONObject) currentA.get("RouteMap")).get("Href");
				StopRegisterVO var = new StopRegisterVO(RouteNo, RouteName, direction, url);

				JSONArray schedules = (JSONArray) currentA.get("Schedules");
				for (Object schedl : schedules) {
					JSONObject currentB = (JSONObject) schedl;
					String pattern = (String) currentB.get("Pattern");
					String destination = (String) currentB.get("Destination");
					String leaveTime = (String) currentB.get("ExpectedLeaveTime");
					Integer countdown = Integer.parseInt((String) currentB.get("ExpectedCountdown").toString());
					String status = (String) currentB.get("ScheduleStatus");
					Boolean cancelledTrip = Boolean.parseBoolean((String) currentB.get("CancelledTrip").toString());
					Boolean cancelledStop = Boolean.parseBoolean((String) currentB.get("CancelledStop").toString());
					Boolean addedTrip = Boolean.parseBoolean((String) currentB.get("AddedTrip").toString());
					Boolean addedStop = Boolean.parseBoolean((String) currentB.get("AddedStop").toString());
					String lastUpdate = (String) currentB.get("LastUpdate");
					ScheduleVO schdl = new ScheduleVO(pattern,destination,leaveTime,countdown,status,cancelledTrip,cancelledStop,addedTrip,addedStop,lastUpdate);
					var.addSchelude(schdl);
				}
				stopRegisters.put(var.getKey(), var);
			}

		}
		catch(Exception e){
		}
	}

	
	public HashTable<TripVO, Integer> getTripsById(Integer stopId)
	{
		HashTable.LinearHash<TripVO, Integer> answer = new HashTable.LinearHash<TripVO, Integer>(3001);
		StructuresIterator<Integer> tripKeys = trips.iterator();
		for(; tripKeys.getCurrent() != null; tripKeys.next())
		{
			StopTimeVO temp = stopTimes.get(stopId + "_" + tripKeys.getElement());
			if( temp != null )
			{
				TripVO enc = trips.get(tripKeys.getElement());
				enc.addStopTime(temp);
				answer.put(enc.getKey(), enc);
			}
		}
		return answer;
	}
	
	public HashTable<StopVO, Integer> getSharedStops( Integer trip1, Integer trip2 )
	{
		HashTable<StopVO, Integer> answer = new HashTable.SeparateChainHash<StopVO, Integer>(6007);
		StructuresIterator<Integer> stopKeys1 = stops.iterator();
		for(; stopKeys1.getCurrent() != null; stopKeys1.next())
		{
			StopTimeVO temp1 = stopTimes.get(stopKeys1.getElement() + "_" + trip1);
			StopTimeVO temp2 = stopTimes.get(stopKeys1.getElement() + "_" + trip2);
			if( temp1 != null && temp2 != null )
			{
				StopVO nueva = stops.get(stopKeys1.getElement());
				temp1.setStop(nueva);
				temp2.setStop(nueva);
				if(temp1.getArrivTime() >= temp2.getArrivTime()) nueva.setArrivalTime(temp1);
				else nueva.setArrivalTime(temp2);
				answer.put(nueva.getKey(), nueva);	
			}
		}
		return answer;
	}
	
	public void sortTrips(HashTable<TripVO, Integer> tripsToSort){
		TripVO[] trips = new TripVO[tripsToSort.size()];
		List<TripVO> ord = new List<TripVO>();
		StructuresIterator<TripVO> iter = tripsToSort.elementsIterator();
		
		for(; iter.getCurrent() != null; iter.next()){
			ord.add(iter.getElement());
		}
		
		ord = ord.sort(trips);
		StructuresIterator<TripVO> listIter = ord.iterator();
	
		System.out.println("Se encontraron " + tripsToSort.size() + " viajes. Sus ids son y tiempo de llegada son: ");
		for(; listIter.getCurrent() != null; listIter.next()){
			System.out.println("Id: "+ listIter.getElement().getTripId() + " - tiempo de llegada:  " + listIter.getElement().getarrt());
		}	
	}
	
	public void sortStops(HashTable<StopVO, Integer> stopsToSort){
		
		StopVO[] stops = new StopVO[stopsToSort.size()];
		List<StopVO> ord = new List<StopVO>();
		StructuresIterator<StopVO> iter = stopsToSort.elementsIterator();
		
		for(; iter.getCurrent() != null; iter.next()){
			ord.add(iter.getElement());
		}
		
		ord = ord.sort(stops);
		StructuresIterator<StopVO> listIter = ord.iterator();
	
		System.out.println("Se encontraron " + stopsToSort.size() + " paradas. Sus ids son:");
		for(; listIter.getCurrent() != null; listIter.next()){
			System.out.println("Id: "+ listIter.getElement().getId());
		}		
	}
	
	boolean getBoolean(Integer x)
	{
		return x == 1;
	}
}
