package model.vo;

public class ServicioVO implements Comparable<ServicioVO>{

	private int serviceId;
	private double distTraveled;
	
	public ServicioVO(int serId){
		serviceId = serId;
		distTraveled = 0;
	}
	
	public int getServiceId(){
		return serviceId;
	}
	public double getDistanceTraveled(){
		return distTraveled;
	}
	
	public void sumDistance(double sum){
		distTraveled =+ sum;
	}
	
	@Override
	public int compareTo(ServicioVO s) {
		double comp = getDistanceTraveled() - s.getDistanceTraveled();
		if(comp > 0) return 1;
		else if(comp < 0) return -1;
		else return 0;
	}
	
}
