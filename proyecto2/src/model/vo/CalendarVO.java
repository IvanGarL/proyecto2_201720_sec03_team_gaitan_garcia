package model.vo;

import java.util.Date;

import model.data_structures.HashTable;
import model.data_structures.StructuresIterator;

public class CalendarVO {

	Integer serviceId;
	boolean monday;
	boolean tuesday;
	boolean wednesday;
	boolean thursday;
	boolean friday;
	boolean saturday;
	boolean sunday;
	Integer startDt;
	Integer endDt;
	HashTable.SeparateChainHash<CalendarDateVO, Integer> dates;
	HashTable.LinearHash<TripVO, Integer> trips;

	public CalendarVO(Integer pServiceId, boolean pMonday, boolean pTuesday, boolean pWednesday, boolean pThursday, boolean pFriday, boolean pSaturday, boolean pSunday, Integer pStartDt, Integer pEndDt){
		serviceId = pServiceId;
		monday = pMonday;
		tuesday = pTuesday;
		wednesday = pWednesday;
		thursday = pThursday;
		friday = pFriday;
		saturday = pSaturday;
		sunday = pSunday;
		startDt = pStartDt;
		endDt = pEndDt;
		dates = new HashTable.SeparateChainHash<CalendarDateVO, Integer>();
		trips = new HashTable.LinearHash<TripVO, Integer>();
	}
	
	public Integer getServiceId(){
		return serviceId;
	}
	public boolean getMonday(){
		return monday;
	}
	public boolean getTuesday(){
		return tuesday;
	}
	public boolean getWednesday(){
		return wednesday;
	}
	public boolean getThursday(){
		return thursday;
	}
	public boolean getFriday(){
		return friday;
	}
	public boolean getSaturday(){
		return saturday;
	}
	public boolean getSunday(){
		return sunday;
	}
	public Integer getStartDt(){
		return startDt;
	}
	public Integer getEndDt(){
		return endDt;
	}
	public int getKey(){
		return serviceId;
	}
	public void loadDates(HashTable.SeparateChainHash<CalendarDateVO, Integer> cdates)
	{
		StructuresIterator<CalendarDateVO> iter = cdates.elementsIterator();
		while( iter.getCurrent() != null )
		{
			CalendarDateVO current = iter.getElement();
			if(current.getServiceId().equals(serviceId)) dates.put(current.getKey(), current);
			iter.next();
		}
	}
	public HashTable.SeparateChainHash<CalendarDateVO, Integer> getDates()
	{
		return dates;
	}
	public void loadTrips(HashTable<TripVO, Integer> pTrips)
	{
		StructuresIterator<TripVO> iter = pTrips.elementsIterator();
		while(iter.getCurrent() != null)
		{
			TripVO current = iter.getElement();
			if( current.getServiceId() == serviceId) trips.put(current.getKey(), current); 
			iter.next();
		}
	}
	public HashTable.LinearHash<TripVO, Integer> getTrips()
	{
		return trips;
	}
}
