package model.vo;

import model.data_structures.HashTable;

public class RangoHoraVO implements Comparable<RangoHoraVO>
{	
	private int horaInicial;
	private int horaFinal;
	HashTable.SeparateChainHash<RetardoVO, Integer> listaRetardos;
	
	public RangoHoraVO(int i, int f)
	{
		horaFinal = f;
		horaInicial = i;
		listaRetardos = new HashTable.SeparateChainHash<RetardoVO, Integer>();
	}

	public int getHoraInicial() {
		return horaInicial;
	}

	public void setHoraInicial(int horaInicial) {
		this.horaInicial = horaInicial;
	}

	public int getHoraFinal() {
		return horaFinal;
	}

	public void setHoraFinal(int horaFinal) {
		this.horaFinal = horaFinal;
	}

	public HashTable.SeparateChainHash<RetardoVO, Integer> getListaRetardos() {
		return listaRetardos;
	}

	public void enqueueRetardo(RetardoVO nuevo) 
	{
		listaRetardos.put(nuevo.getKey(), nuevo);
	}

	@Override
	public int compareTo(RangoHoraVO o) {
		// TODO Auto-generated method stub
		if(listaRetardos.size() < o.getListaRetardos().size())
		{
			return -1;
		}
		else if(listaRetardos.size() > o.getListaRetardos().size())
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}
