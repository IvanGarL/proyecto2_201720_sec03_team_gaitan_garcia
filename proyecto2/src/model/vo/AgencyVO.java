package model.vo;

import model.data_structures.HashTable;
import model.data_structures.HashList;
import model.data_structures.StructuresIterator;

public class AgencyVO {
	
  String id;
  String name;
  String agenUrl;
  String timeZone;
  String language;
  
  HashTable.SeparateChainHash<RouteVO, Integer> routes;
  
  public AgencyVO(String pId, String pName, String pUrl, String tZone, String lang){
	id = pId;
	name = pName;
	agenUrl = pUrl;
	timeZone = tZone;
	language = lang;
	routes = new HashTable.SeparateChainHash<RouteVO, Integer>();
  }
  
  public String getId(){
	  return id;
  }
  public String getName(){
	  return name;
  }
  public String getUrl(){
	  return agenUrl;
  }
  public String getTimeZone(){
	  return timeZone;
  }
  public String getLanguage(){
	  return language;
  }
  public String getKey(){
	  return id;
  }
  public void loadRoutes(HashTable.SeparateChainHash<RouteVO, Integer> pRoutes){
		  StructuresIterator<RouteVO> iter = pRoutes.elementsIterator();
		  while(iter.getCurrent() != null)
		  {
			  RouteVO current = iter.getElement();
			  if( current.getAgencyId().equalsIgnoreCase(id) ) current.setAgencyName(this.name); routes.put(current.getKey(), current); 
			  iter.next();
		  }
	}
	public HashTable.SeparateChainHash<RouteVO, Integer> getRoutes()
	{
		return routes;
	}
}
