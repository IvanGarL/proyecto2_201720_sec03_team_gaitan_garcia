package model.vo;

public class RetardoVO implements Comparable <RetardoVO>{
		
	private int tripId;
	private double tiempoRetardo;
	
	public RetardoVO(int pTripId, double time){
		tripId = pTripId;
		tiempoRetardo = time;
	}
	
	public int getTripId(){
		return tripId;
	}
	public double getTiempoRetardo(){
		return tiempoRetardo;
	}
	public void setTiempoRetardo(double time){
		tiempoRetardo = time;
	}
	public int getKey(){
		return tripId;
	}
	@Override
	public int compareTo(RetardoVO rt) {
		// TODO Auto-generated method stub
		double comp = getTiempoRetardo() - rt.getTiempoRetardo();
		if(comp > 0) return 1;
		else if(comp < 0) return -1;
		else return 0;
	}
		
}