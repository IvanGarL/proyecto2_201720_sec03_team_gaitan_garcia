package model.vo;

public class ScheduleVO {

	String pattern;
	String destination;
	String leaveTime;
	Integer countdown;
	String status;
	Boolean cancelledTrip;
	Boolean cancelledStop;
	Boolean addedTrip;
	Boolean addedStop;
	String lastUpdate;

	public ScheduleVO(String pPattern, String pdestination, String petime, Integer pecountdwon, String pstatus, Boolean canTrip, Boolean canStop, Boolean adTrip, Boolean adStop, String plastupdate){
		pattern = pPattern;
		destination = pdestination;
		leaveTime = petime;
		countdown = pecountdwon;
		status = pstatus;
		cancelledTrip = canTrip;
		cancelledStop = canStop;
		addedTrip = adTrip;
		addedStop = adStop;
		lastUpdate = plastupdate;
	}

	public String getPattern(){
		return pattern;
	}
	public String getDestination(){
		return destination;
	}
	public String getLeaveTime(){
		return leaveTime;
	}
	public Integer getCountdown(){
		return countdown;
	}
	public String getStatus(){
		return status;
	}
	public Boolean getCancelledTrip(){
		return cancelledTrip;
	}
	public Boolean getCancelledStop(){
		return cancelledStop;
	}
	public Boolean getAddedTrip(){
		return addedTrip;
	}
	public Boolean getAddedStop(){
		return addedStop;
	}
	public String  getLastUpdate(){
		return lastUpdate;
	}
	public String getKey(){
		return pattern + lastUpdate;
	}
	public String getDate()
	{
		String s = leaveTime.split(" ")[1];
		return s.split("-")[0] + s.split("-")[1] + s.split("-")[2] ;
	}
}
