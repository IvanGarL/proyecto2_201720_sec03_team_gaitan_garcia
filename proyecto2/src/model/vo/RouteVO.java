package model.vo;

import java.awt.Color;
import java.net.URL;

import model.logic.STSManager;
import model.data_structures.HashTable;
import model.data_structures.StructuresIterator;

public class RouteVO implements Comparable<RouteVO>
{	
	private int id;
	private String agencyId;
	private String routeNum;
	private String routeName;
	private String routeDesc;
	private String routeUrl;
	private Color routeColor;
	private Color routeTextColor;
	private String agencyName;
	private HashTable.SeparateChainHash<TripVO, Integer> trips;
	private int numberOfStops;
	public RouteVO( int pId, String pAngencyId, String pRouteNum, String pRouteName, String pRouteDesc, String pUrl, Color pRouteColor, Color pRouteText)
	{
		id = pId;
		agencyId = pAngencyId;
		routeDesc = pRouteDesc;
		routeColor = pRouteColor;
		routeTextColor = pRouteText;
		routeUrl = pUrl;
		routeNum = pRouteNum;
		routeName = pRouteName;
		trips = new HashTable.SeparateChainHash<TripVO, Integer>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getRouteNum() {
		return routeNum;
	}

	public void setRouteNum(String routeNum) {
		this.routeNum = routeNum;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getRouteUrl() {
		return routeUrl;
	}

	public void setRouteUrl(String routeUrl) {
		this.routeUrl = routeUrl;
	}

	public Color getRouteColor() {
		return routeColor;
	}

	public void setRouteColor(Color routeColor) {
		this.routeColor = routeColor;
	}

	public Color getRouteTextColor() {
		return routeTextColor;
	}

	public void setRouteTextColor(Color routeTextColor) {
		this.routeTextColor = routeTextColor;
	}
	public int getKey(){
		return id;
	}
	public String toString()
	{
		return id + "";
	}
	public void loadTrips(HashTable<TripVO,Integer> pTrips)
	{
		  StructuresIterator<TripVO> iter = pTrips.elementsIterator();
		  while(iter.getCurrent() != null)
		  {
			  TripVO current = iter.getElement();
			  if( current.getRouteId() == id ) trips.put(current.getKey(), current);
			  iter.next();
		  }
	}
	public HashTable.SeparateChainHash<TripVO, Integer> getTrips()
	{
		return trips;
	}
	public String getAgencyName()
	{
		return agencyName;
	}
	public void setAgencyName(String pName)
	{
		agencyName = pName;
	}
//	public int getNumberOfStops()
//	{
//		StructuresIterator<TripVO> t = trips.elementsIterator();
//		while( t != null)
//		{
//			t.getElement().loadTimes(STSManager.stopTimes);
//			numberOfStops += t.getElement().getStopTimes().getSize();
//			t.getElement().reset();
//			t.next();
//		}
//		return numberOfStops;
//	}
	
	@Override
	public int compareTo(RouteVO r) {
		int comp = getId() - r.getId();
		if(comp > 0) return 1;
		else if(comp < 0) return -1;
		else return 0;
	}
	
	public int compareTo(int tam)
	{
		if(numberOfStops < tam )
			return -1;
		else if( numberOfStops > tam)
			return 1;
		else
			return 0;
	}
}

