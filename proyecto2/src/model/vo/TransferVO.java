package model.vo;

import model.data_structures.IList;

import model.vo.StopVO;

public class TransferVO
{
	private String startStopId;
	private String endStopId;
	private Integer transferType;
	
	public TransferVO(String start, String end, Integer ttype, Integer ttime ){
		startStopId = start;
		endStopId = end;
		transferType = ttype;
		transferTime = ttime;
	}
	
	public String getStartId(){
		return startStopId;
	}
	public String getEndId(){
		return endStopId;
	}
	public Integer getTransferType(){
		return transferType;
	}

	/**
	 * Modela el tiempo de transbordo
	 */
	private int transferTime;
	
	/**
	 * Lista de paradas que conforman el transbordo
	 */
	private IList<StopVO> listadeParadas;

	/**
	 * @return the transferTime
	 */
	public int getTransferTime()
	{
		return transferTime;
	}

	/**
	 * @param transferTime the transferTime to set
	 */
	public void setTransferTime(int transferTime) 
	{
		this.transferTime = transferTime;
	}
	
	public int getKey(){
		return Integer.parseInt(startStopId + endStopId);
	}

	/**
	 * @return the listadeParadas
	 */
	public IList<StopVO> getListadeParadas()
	{
		return listadeParadas;
	}

	/**
	 * @param listadeParadas the listadeParadas to set
	 */
	public void setListadeParadas(IList<StopVO> listadeParadas) 
	{
		this.listadeParadas = listadeParadas;
	}
	

}
