package model.vo;

import model.data_structures.IList;
import model.data_structures.List;

public class StopPlanVO implements Comparable<StopPlanVO>
{
	StopVO stop;
	private IList<TripVO> trips;
	private IList<RouteVO> routes;
	
	public StopPlanVO( StopVO stop )
	{
		trips = new List<TripVO>();
		routes = new List<RouteVO>();
		this.stop = stop;
	}

	public StopVO getStop() 
	{
		return stop;
	}

	public IList<TripVO> getTrips() 
	{
		return trips;
	}

	public IList<RouteVO> routes() 
	{
		return routes;
	}
	public void setStop( StopVO pStop) 
	{
		stop = pStop;
	}
	public void addTrip(TripVO nuevo)
	{
		trips.add(nuevo);
	}
	public void addRoute(RouteVO nuevo)
	{
		routes.add(nuevo);
	}

	@Override
	public int compareTo(StopPlanVO sp) {
		// TODO Auto-generated method stub
		int comp = getStop().getId() - sp.getStop().getId();
		if(comp > 0) return 1;
		else if (comp < 0) return -1;
		else return 0;
	}
}
