package model.vo;


import model.data_structures.HashTable;
import model.data_structures.StructuresIterator;

public class CalendarDateVO{

	Integer id;
	Integer exceptionDt;
	Integer type;
	HashTable.SeparateChainHash<TripVO, Integer> trips;
	public CalendarDateVO(Integer pId, Integer exception,Integer pType){
		id = pId;
		exceptionDt = exception;
		type = pType;
		trips = new HashTable.SeparateChainHash<TripVO, Integer>();
	}

	public Integer getServiceId(){
		return id;
	}
	public Integer getExceptionDate(){
		return exceptionDt;
	}
	public Integer getType(){
		return type;
	}
	public int getKey(){
		return id;
	}
	public void loadTrips(HashTable<TripVO, Integer> pTrips)
	{
		  StructuresIterator<TripVO> iter = pTrips.elementsIterator();
		  while(iter.getCurrent() != null)
		  {
			  TripVO current = iter.getElement();
			  if( current.getServiceId() == id ) trips.put(current.getKey(), current);
			  iter.next();
		  }
	}
	public HashTable.SeparateChainHash<TripVO, Integer> getTrips()
	{
		return trips;
	}
}
