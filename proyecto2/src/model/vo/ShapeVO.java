package model.vo;

public class ShapeVO {

	Integer id;
	double latitude;
	double longitude;
	Integer sequence;
	Double disTraveled;
	
	public ShapeVO(Integer pId, Double pLat, Double pLong, Integer pSeq, Double pDisTravel){
		id = pId;
		latitude = pLat;
		longitude = pLong;
		sequence = pSeq;
		disTraveled = pDisTravel;
	}
	
	public Integer getId(){
		return id;
	}
	public Double getLatitude(){
		return latitude;
	}
	public Double getLongitude(){
		return longitude;
	}
	public Integer getSequence(){
		return sequence;
	}
	public Double getDistanceTraveled(){
		return disTraveled;
	}
	public int getKey(){
		return id;
	}
}
