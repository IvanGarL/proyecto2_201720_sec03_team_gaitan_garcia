package model.vo;

import model.data_structures.List;
import model.data_structures.Queue;
//import model.exceptions.DateNotFoundException;
import model.data_structures.HashTable;
import model.data_structures.IList;

public class StopRegisterVO {

	String routeNumber;
	String routeName;
	String direction;
	String url;
	
	HashTable.LinearHash<ScheduleVO, String> schedules;
	
	public StopRegisterVO(String prouteNumber, String prouteName, String pdirection, String purl ){
		schedules = new HashTable.LinearHash<ScheduleVO, String>();
		routeNumber = prouteNumber;
		routeName = prouteName;
		direction = pdirection;
		url = purl;
	}
	
	public String getRouteNumber(){
		return routeNumber;
	}
	public String getRouteName(){
		return routeName;
	}
	public String getDirection(){
		return direction;
	}
	public String getUrl(){
		return url;
	}
	public String getKey(){
		return routeName + routeNumber;
	}
	public void addSchelude(ScheduleVO nSchelude){
		schedules.put(nSchelude.getKey(), nSchelude);
	}
//	public ScheduleVO getScheduleAtK(int pos) throws DateNotFoundException
//	{
//		return schedules.getElementAtK(pos);
//	}
	
}
