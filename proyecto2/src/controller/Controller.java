package controller;

import java.io.File;

import model.data_structures.HashTable;
import model.data_structures.IList;
//import model.exceptions.ElementNotFoundException;
import model.logic.STSManager;
import model.vo.StopVO;
import model.vo.TripVO;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();
	public static void loadRoutes() 
	{
		manager.loadRoutes();
	}

	public static void loadTrips() {
		manager.loadTrips();
	}

	public static void loadStopTimes() {
		manager.loadStopTimes();
	}
	public static IList<StopVO> stopsRoute(String routeName, String direction) {
		return null;
	}

	public static void loadTransfers() {
		manager.loadTransfers();		
	}

	public static void loadStops() {
		manager.loadStops();		
	}
	public static void loadShapes() {
		manager.loadShapes();		
	}
	public static void loadCalendar() {
		manager.loadCalendar();		
	}
	public static void loadCalendarDates() {
		manager.loadCalendarDates();		
	}
	public static void loadAgencies() {
		manager.loadAgencies();		
	}
	public static void readBusUpdates() {
		File f = new File("data/JSON/Buses");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readBusUpdate(updateFiles[i]);
		}
	}
	public static void readStopRegisters(){
		File f = new File("data/JSON/StopRegisters");
		File[] updateFiles = f.listFiles();
		for (int i = 0; i < updateFiles.length; i++) {
			manager.readStopRegistersUpdate(updateFiles[i]);
		}	}
	
	public static HashTable<TripVO, Integer> getTripsById(Integer stopId) 
	{
		return manager.getTripsById(stopId);
	}
	public static void sortTrips(HashTable<TripVO, Integer> answer){
		manager.sortTrips(answer);
	}
	
	public static void sortStops(HashTable<StopVO, Integer> answer){
		manager.sortStops(answer);
	}
	
	public static HashTable<StopVO, Integer> getSharedStops(Integer trip1, Integer trip2) {
		// TODO Auto-generated method stub
		return manager.getSharedStops( trip1,  trip2);
	}
	
}
