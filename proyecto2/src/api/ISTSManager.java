package api;

import java.io.File;

import model.data_structures.HashTable;
import model.vo.StopVO;
import model.vo.TripVO;


public interface ISTSManager {

	public void loadTransfers();
	public void loadAgencies();
	public void loadShapes();
	public void loadCalendar();
	public void loadCalendarDates();

	/**
	 * Method to load the routes of the STS
	 * @param routesFile - path to the file 
	 */
	public void loadRoutes();
	
	/**
	 * Method to load the trips of the STS
	 * @param tripsFile - path to the file 
	 */
	public void loadTrips();
	
	/**
	 * Method to load the times bus trips stop at a stop in the STS
	 * @param stopTimesFile - path to the file 
	 */
	public void loadStopTimes();
	
	/**
	 * Method to load the stops of the STS
	 * @param stopsFile - path to the file 
	 */
	public void loadStops();
	
	
	/**
	 * Reads a file in json format containing bus updates in real time
	 * @param rtFile - The file to read
	 */
	public void readBusUpdate(File rtFile);

	/**
	 * Lee los json de stop registers
	 * @param file
	 */
	public void readStopRegistersUpdate(File file);
	public HashTable<TripVO, Integer> getTripsById(Integer stopId);

	public HashTable<StopVO, Integer> getSharedStops(Integer trip1, Integer trip2);
	
	public void sortTrips(HashTable<TripVO, Integer> tripsToSort);

	public void sortStops(HashTable<StopVO, Integer> stopsToSort);

}
