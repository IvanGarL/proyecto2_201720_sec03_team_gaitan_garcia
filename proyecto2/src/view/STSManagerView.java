package view;

import java.util.Scanner;

import model.data_structures.HashTable;
import model.data_structures.StructuresIterator;
import model.vo.StopVO;
import model.vo.TripVO;
import controller.Controller;

public class STSManagerView {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			case 1:
				System.out.println("Cargando los datos por favor espere, esta acción puede tomar unos segundos... ");
				Controller.loadTrips();
				Controller.loadRoutes();
				Controller.loadStops();
				Controller.loadStopTimes();
				Controller.loadCalendarDates();
				Controller.loadCalendar();
				Controller.loadAgencies();
				Controller.loadTransfers();
				Controller.loadShapes();
				break;
			case 2:
				System.out.println("Cargando los datos por favor espere, esta acción puede tomar unos segundos... ");
				Controller.readBusUpdates();
				Controller.readStopRegisters();
			case 3:
				double startTime = System.currentTimeMillis();
				System.out.println("Cargando los datos por favor espere, esta acción puede tomar unos segundos... ");
				Controller.loadTrips();
				Controller.loadRoutes();
				Controller.loadStops();
				Controller.loadStopTimes();
				Controller.loadCalendarDates();
				Controller.loadCalendar();
				Controller.loadAgencies();
				Controller.loadTransfers();
				Controller.readBusUpdates();
				Controller.readStopRegisters();
				Controller.loadShapes();
				System.out.println("Los datos se han cargado exitosamente!");
				double endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 4:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("1. Cargar Datos");
		System.out.println("2. Encontrar los viajes que pasan por una parada");
		System.out.println("3. Encontrar las paradas que hacen parte de dos viajes diferentes");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
}
