package src.model.data_structures.tests;

import junit.framework.TestCase;
import model.data_structures.HashTable;

public class ChainHashTest extends TestCase
{
	HashTable<String, Integer> ht;
	public void setUp()
	{
		ht = new HashTable.SeparateChainHash<String, Integer>();
	}
	public void setUp2()
	{
		ht = new HashTable.SeparateChainHash<String, Integer>(8);
		for(int i = 0; i < 25; i++)
			ht.put(i, "aaa"+i);
	}
	public void testPut()
	{
		ht.put(22, "aaaaa22");
		assertEquals("aaaaa22", ht.get(22));
		ht.put(23, "aaaaa23");
		assertEquals("aaaaa23", ht.get(23));
		ht.put(24, "aaaaa24");
		assertEquals("aaaaa24", ht.get(24));
		ht.put(25, "aaaaa25");
		assertEquals("aaaaa25", ht.get(25));
		ht.put(26, "aaaaa26");
		assertEquals("aaaaa26", ht.get(26));
		ht.put(26, "aaaaa27");
		assertEquals("aaaaa26", ht.get(26));
	}
	public void testSize()
	{
		setUp();
		assertEquals(0, ht.size());
		ht.put(1, "aa1");
		assertEquals(1, ht.size());
		setUp2();
		assertEquals(25, ht.size());
	}
	public void testGet()
	{
		setUp();
		assertNull(ht.get(11));
		setUp2();
		assertNull(ht.get(25));
		assertEquals("aaa0", ht.get(0));
		assertEquals("aaa8", ht.get(8));
		assertEquals("aaa16", ht.get(16));
		assertEquals("aaa24", ht.get(24));
		assertEquals("aaa1", ht.get(1));
		assertEquals("aaa9", ht.get(9));
		assertEquals("aaa17", ht.get(17));
	}
}
