package src.model.data_structures.tests;

import junit.framework.TestCase;
import model.data_structures.List;

public class ListTest extends TestCase
{
	List<Integer> numbers;
	public void setUp()
	{
		numbers = new List<Integer>();
	}
	public void setUp2( )
	{
		numbers = new List<Integer>();
		numbers.add(0);
	}
	public void setUp3( )
	{
		numbers = new List<Integer>();

		for(int i = 0; i < 10; i++){
			numbers.add(i);
		}
	}
	/**
	 * Prueba el metodo que agrega al final
	 * Caso 1: Lista vacia
	 * Caso 2: Lista con 1 elemento
	 * Caso 3: Lista con 0 o mas elementos
	 */
	public void testAdd()
	{
		setUp();
		numbers.add(0);
		assertEquals("Deberian ser iguales", (Integer) 0, numbers.getFirst());

		numbers.add(1);
		assertEquals("Deberian ser iguales", (Integer) 0, numbers.getFirst());
		assertEquals("Deberian ser iguales", (Integer) 1, numbers.getLast());

		numbers.add(2);
		assertEquals("Deberian ser iguales", (Integer) 0, numbers.getFirst());
		assertEquals("Deberian ser iguales", (Integer) 2, numbers.getLast());
	}
	/**
	 * Prueba el metodo que agrega en una posicion
	 * Caso 1: Lista vacia
	 * Caso 2: Lista con 1 elemento
	 * Caso 3: Posicion inicial
	 * Caso 4: Posicion final
	 * Caso 5: En la mitad
	 */
	public void testAddAtK()
	{
		setUp();
		numbers.addAtK(0, 0);
		assertEquals("Deberian ser iguales", (Integer) 0, numbers.getFirst());
		numbers.addAtK(1, 1);
		assertEquals("Deberian ser iguales", (Integer) 1, numbers.getLast());
		numbers.addAtK(0, 2);
		assertEquals("Deberian ser iguales", (Integer) 2, numbers.getFirst());
		numbers.addAtK(3, 3);
		assertEquals("Deberian ser iguales", (Integer) 3, numbers.getLast());
		numbers.addAtK(2, 4);
		assertEquals("Deberian ser iguales", (Integer) 4, numbers.getElementAtK(2));
	}
	/**
	 * Prueba el metodo que elimina en una posicion
	 * Caso 1: Lista con 1 elemento
	 * Caso 2: Varios elementos en la mitad
	 * Caso 3: Posicion final
	 * Caso 4: Posicion inicial
	 */
	public void testDeleteAtK()
	{
		setUp2();
		numbers.deleteAtK(0);
		assertNull("No deberia de haber elementos", numbers.getFirstNode());
		assertNull("No deberia de haber elementos", numbers.getLastNode());
		
		setUp3();
		numbers.deleteAtK(0);
		assertEquals("Deberian ser iguales", (Integer) 1, numbers.getElementAtK(0));
		numbers.deleteAtK(8);
		for(int i = 0; i < numbers.getSize(); i++) 	System.out.println(numbers.getElementAtK(i));
		System.out.println(numbers.getLast());
		assertEquals("Deberian ser iguales", (Integer) 8, numbers.getLast());
		numbers.deleteAtK(3);
		assertEquals("Deberian ser iguales", (Integer) 5, numbers.getElementAtK(3));
		
	}
	/**
	 * Prueba el metodo que elimina en una posicion
	 * Caso 1: Elemento en la mitad
	 * Caso 2: Posicion final
	 * Caso 3: Posicion inicial
	 */
	public void testGetAtK()
	{
		setUp3();
		assertEquals("Deberian ser iguales", (Integer) 3, numbers.getElementAtK(3));
		assertEquals("Deberian ser iguales", (Integer) 0, numbers.getElementAtK(0));
		assertEquals("Deberian ser iguales", (Integer) 9, numbers.getElementAtK(9));

	}
}
